
# Classic 2-digit decimal Counter

Here is trivial 2-digit decimal counter with LED display.
It uses common CMOS 4000 IOs only - no programming
required - just connect all parts together and add 5V Power.

## Setup

Schematics has been created using free
ExpressSCH 7.0.2 (was included in ExpressPCB package).  

You can get ExpressSCH from https://www.expresspcb.com/expresssch/

## Bugs

There should be Power-on RESET circuit to ensure that counter
are in defined state on Power-up.

## Resources

There were used following resources from other authors:

* http://www.learningaboutelectronics.com/Articles/Astable-multivibrator-with-a-4011-NAND-gate.php - original Astable Flip-Flop to generate clock signal.
  I made following modifications:
  * increased resistor 10x (CMOS unlike TTL has very low input current,
    thus 10k resistor would draw overkill current)
  * decreased capacitor from 100uF to 1uF (thanks to incresed resistor value)

* http://tronixstuff.com/tag/4518/ - example circuit with CMOS counter 4518

* http://www.nutsvolts.com/magazine/article/using-seven-segment-displays-part-2
  example using 4543 to driver 7-segment LED display.


